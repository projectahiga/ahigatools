import geometry;
unitsize(1mm);

point C0 = (0,0);
point C1 = (70,0);

circle cbase = circle(C0, 18);

line vtan[] = tangents(cbase,C1);
draw(vtan[0],blue);
draw(cbase);
draw((point)(-20,-20),white);
draw((point)(80,20),white);
