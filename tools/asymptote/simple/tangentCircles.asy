import geometry;
unitsize(1mm);

point C0 = (0,0);
point C1 = (70,0);

circle cbase = circle(C0, 18);
circle cxout = circle(C1, 8);
circle caux  = circle(C0, 8);

line vtan[] = tangents(caux,C1);
point inter1[] = intersectionpoints(vtan[0], caux);
line auxL = line(C0,false, inter1[0]);
point inter2[] = intersectionpoints(auxL, cbase);
line vtan2[] = tangents(cxout,inter2[0]);
draw(vtan2[0],blue);
draw(cbase);
draw(cxout);
