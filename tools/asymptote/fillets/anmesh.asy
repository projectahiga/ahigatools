/*
  Copyright (c) 2018 Roman Putanowicz <putanowr@gmail.com> 

  Permission is hereby granted, free of charge, to any person
  obtaining a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

/*
  Basic handling of unstructured meshed in 2D. 
*/

private import geometry;

struct Vertex {
  point xy;
  void draw() {
    dot(this.xy, red+6);
  }
  void operator init(real x, real y) {
    this.xy = (point)(x, y);
  }  
}

struct Cell {
  Vertex[] vref;
  int[] vertices;
  path p;
  void draw(pen p=defaultpen) {
    if (this.p == nullpath) {
      for (int i : this.vertices) {
        this.p = this.p--(this.vref[i].xy);
      }
      this.p=this.p--cycle;
    }
    filldraw(this.p, yellow, p+black);
  }
  void operator init(int[] verts, Vertex[] vr) {
    this.vertices = copy(verts);
    this.vref = vr;
  }
}

struct Adjacency {
  int[] sources;
  int[][] targets;

  void add(int[] t) {
    int vl =  this.sources.length;
    this.sources.push(vl); 
    this.targets.push(copy(t));
  }
  void write() {
    for (int i = 0; i<this.targets.length; ++i) {
       write(format("%d: ",i),none);
       for(int k : this.targets[i]) {
         write(format(" %d", k), none);
       }    
       write("");
    }
  }
  void operator init() {
  }
}

struct EdgeGeom {
  string type; 
  path p;
  arc a;
  void operator init(arc a) {
    this.type = "arc";
    this.a = a;
  }
  void operator init(path p) {
    this.type = "path";
    this.p = p;
  }
  void operator init() {
    this.type = "segment";
  }
}

struct Mesh2D {
  Vertex[] vertices;
  Cell[] cells; 
  int dim;

  void draw(pen p = defaultpen) {
    for (Cell c : this.cells) {
      c.draw(p);
    }
    for (Vertex v : this.vertices) {
      v.draw();
    }
  }
  void labelVertices(pen p) {
    int i=0;
    for (Vertex v : this.vertices) {
      pen lp = p+blue;
      label(format("%d",i), v.xy, align=NE, lp);
      i=i+1;
    }
  }
  int addVertex(real x, real y) {
    Vertex z = this.vertices.push(Vertex(x,y));
    return vertices.length;
  }
  void printVertices() {
    for (int i = 0; i< this.vertices.length; ++i) {
      write("Vertex ", i, this.vertices[i].xy.x, this.vertices[i].xy.y);
    }
  }
  int addCell(int[] verts) {
    int vl = verts.length;
    if (vl != 3 && vl != 4) {
      abort(format("Can add cell only with 3 or 4 vertices, got %d", vl));
    }
    int nn = this.vertices.length;
    for (int k : verts) {
      if (k >= nn) {
        abort(format("Vertex index exceeds number of mesh vertices %d",k));
      }
    } 
    this.cells.push(Cell(verts, this.vertices));
    return cells.length;
  }
  void setVertex(int id, point p) {
    int vl = this.vertices.length;
    if (id >= vl) {
      abort(format("Vertex id out of range [0, %d)", vl));
    }
    this.vertices[id].xy = (point)(p.x, p.y);
  }
  void readRaw(string filename) {
    file fin = input(filename);
    if (error(fin)) {
      string msg = "Cannot find file" + filename;
      abort(msg);
    }
    int nnodes = fin;
    int nelements = fin;
    for (int i=0; i<nnodes; ++i) {
      real[] xyz = fin.dimension(3);
      this.addVertex(xyz[0], xyz[1]);
    }
    for (int k = 0; k < nelements; ++k) {
      int nen = fin;
      int[] verts = fin.dimension(nen);
      this.addCell(verts);
    }
  }
  void operator init() {
    this.dim = 2;
  } 
}
