unitsize(2cm);
import anmesh;

string fname;
usersetting();

Mesh2D mesh;
mesh.readRaw(fname);
mesh.draw(defaultpen+2);
mesh.labelVertices(fontsize(18));
