import geometry;
unitsize(1mm);

segment circseg(circle A, circle B)
{
  circle caux = circle(A.C, B.r);
  line taux[] = tangents(caux, B.C);
  point PAaux[] = intersectionpoints(taux[0], caux);
  line haux = line(A.C, false, PAaux[0]);
  point PSA[] = intersectionpoints(haux, A);
  line baux[] = tangents(B, PSA[0]);
  point PSB[] = intersectionpoints(baux[1], B);
  return segment(PSA[0], PSB[0]);
}
circle cbase = circle((point)(0,0), 18);
circle cyout = circle((point)(0,70), 8);
circle cxout = circle((point)(-50,0), 8);
circle cyin  = circle((point)(0,70),3);
circle cxin  = circle((point)(-50,0),3);
circle caux  = circle((point)(0,0), 8);
circseg(cxin, caux);
point C = (0,0);
point Y = (0,70);
line vtan[] = tangents(caux,Y);
point C1[] = intersectionpoints(vtan[0], caux);
dot(C1[0], green);
line auxLC1 = line(C,false, C1[0]);
point C2[] = intersectionpoints(auxLC1, cbase);
line vtan2[] = tangents(cyout,C2[0]);
draw(vtan2[1],blue);
draw(auxLC1, red);
draw(cbase);
draw(cyout);
draw(cxout);
draw(cyin);
draw(cxin);
draw(caux, red);
dot(Y);
dot(C);
draw(vtan[0], red);
