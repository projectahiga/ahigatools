private import geometry;

struct Fillet {
  segment sA;
  segment sB;
  arc arc;
  real radius;
  point center;
  point[] pts;

  void operator init(segment left, segment right, real R) {
    point ac = left.B - R*unit(left.B-left.A);
    point bc = right.B - R*unit(right.B-right.A);
    point ma = rotate(-90, ac)*left.B;
    point mb = rotate(90, bc)*right.B;
    line lAp = parallel(ma, left);
    line lBp = parallel(mb, right);
    this.center = intersectionpoint(lAp, lBp);
    point leftTP = projection(left)*this.center;
    point rightTP = projection(right)*this.center;
    circle auxcirc = circle(this.center, R);
    this.sA = segment(left.A, leftTP);
    this.sB = segment(right.A, rightTP);
    this.arc = arc(auxcirc, rightTP, leftTP);
    this.radius = R;
    this.pts.push(right.A);
    this.pts.push(rightTP);
    this.pts.push(leftTP);
    this.pts.push(left.A);
  }
  void draw(pen p) {
    draw(this.arc, p);
    draw(this.sA, p);
    draw(this.sB, p);
  }
  path getPath() {
    return this.sB.A--this.sB.B..(path)this.arc..this.sA.B--this.sA.A--cycle;
  }
}

segment makeTangentSegment(circle A, circle B, string side)
{
  int sf = 0; 
  if (side == "left") {
    sf = -1;
  } else if (side == "right") {
    sf = 1;
  } else {
    abort("Invalid name of side");
  }
  line ax = line(A.C, B.C);
  bool reversed = false;
  if (A.r < B.r) {
    circle C = B;
    B = A;
    A = C;
    reversed = true;
  }
  circle caux = circle(A.C, A.r-B.r);
  line taux[] = tangents(caux, B.C);
  real an = sharpangle(ax, taux[0]);
  line tax;
  if (sf*an > 0) {
    tax = taux[0];
  } else {
    tax = taux[1];
  }
  point PAaux[] = intersectionpoints(tax, caux);
  line haux = line(A.C, false, PAaux[0]);
  point PSA[] = intersectionpoints(haux, A);
  line baux[] = tangents(B, PSA[0]);
  int s=1;
  if (parallel(tax, baux[0])) {
    s = 0;
  }
  point PSB[] = intersectionpoints(baux[s], B);
  segment tseg = segment(PSA[0], PSB[0]);
  if (reversed) {
    tseg = reverse(tseg);
  }
  return tseg;
}
