import circgeom;
import geometry;
unitsize(1mm);

//---------------------------------------------------------
// Parameters governing the geometry
//---------------------------------------------------------
real H = 40;
real W = 50;
real R = 5;
real r = 14;
real rHole = 3;
real rFillet = 6;

//---------------------------------------------------------
// Construction of geometric entities
//---------------------------------------------------------
circle cbase = circle((point)(0,0), R);
circle cFilletN = circle((point)(0,H), r);
circle cFilletW = circle((point)(-W,0), r);
circle cHoleN  = circle((point)(0,H), rHole);
circle cHoleW  = circle((point)(-W,0), rHole);
segment sN1 = makeTangentSegment(cbase, cFilletN, "left");
segment sN2 = makeTangentSegment(cbase, cFilletN, "right");
segment sW1 = makeTangentSegment(cbase, cFilletW, "left");
segment sW2 = makeTangentSegment(cbase, cFilletW, "right");
Fillet fc1 = Fillet(reverse(sN1), reverse(sW2), rFillet);
Fillet fc2 = Fillet(reverse(sN2), reverse(sW1), R);
arc aN = arc(cFilletN, sN2.B, sN1.B);
arc aW = arc(cFilletW, sW2.B, sW1.B);
if (r>R) { 
  aN = complementary(aN);
  aW = complementary(aW);
}

//---------------------------------------------------------
// Drawing
//---------------------------------------------------------
fc1.draw(black);
fc2.draw(black);
draw(aW);
draw(aN);
draw(cHoleN);
draw(cHoleW);
dot(fc1.pts);
dot(fc2.pts);
