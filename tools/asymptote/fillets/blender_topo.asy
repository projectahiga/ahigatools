import circgeom;
import geometry;
import anmesh;

unitsize(3mm);

string fname;
usersetting();


//---------------------------------------------------------
// Parameters governing the geometry
//---------------------------------------------------------
real H = 70;
real W = 50;
real R = 18;
real r = 8;
real rHole = 3;
real rFillet = 4;

//---------------------------------------------------------
// Construction of geometric entities
//---------------------------------------------------------
circle cbase = circle((point)(0,0), R);
circle cFilletN = circle((point)(0,H), r);
circle cFilletW = circle((point)(-W,0), r);
circle cHoleN  = circle((point)(0,H), rHole);
circle cHoleW  = circle((point)(-W,0), rHole);
segment sN1 = makeTangentSegment(cbase, cFilletN, "left");
segment sN2 = makeTangentSegment(cbase, cFilletN, "right");
segment sW1 = makeTangentSegment(cbase, cFilletW, "left");
segment sW2 = makeTangentSegment(cbase, cFilletW, "right");
Fillet fc1 = Fillet(reverse(sN1), reverse(sW2), rFillet);
Fillet fc2 = Fillet(reverse(sN2), reverse(sW1), R);
arc aN = arc(cFilletN, sN2.B, sN1.B);
arc aW = arc(cFilletW, sW2.B, sW1.B);
if (r>R) { 
  aN = complementary(aN);
  aW = complementary(aW);
}

//---------------------------------------------------------
// Mesh of IGA pathes
//---------------------------------------------------------
Mesh2D mesh;
mesh.readRaw(fname);

// Impose geometry on patches mesh
abscissa midarc=relabscissa(0.5);


// Return point along a segment given the offset from segment starting point
point segpoint(segment s, real offset) {
  return s.A*(1-offset)+s.B*offset;
}

// Find inresecion of a circle with ray from circle center through point p 
point raypoint(circle c, point p) {
  point[] ip = intersectionpoints(c, line(c.C, false, p, true));
  return ip[0];
}

mesh.setVertex(0, 0.5*(fc1.pts[2]+fc2.pts[2]));
mesh.setVertex(1, fc1.pts[2]);
mesh.setVertex(2, segpoint(fc1.sA, 0.4));
mesh.setVertex(3, angpoint(cHoleN, 270));
mesh.setVertex(4, segpoint(fc2.sA, 0.4));
mesh.setVertex(5, fc2.pts[2]);
mesh.setVertex(6, fc1.pts[3]);
mesh.setVertex(7, raypoint(cHoleN, fc1.pts[3]));
mesh.setVertex(8, raypoint(cHoleN, fc2.pts[3]));
mesh.setVertex(9, fc2.pts[3]);
mesh.setVertex(10, point(aN, midarc));
mesh.setVertex(11, angpoint(cHoleN, 90));
mesh.setVertex(12, segpoint(fc2.sB, 0.2));
mesh.setVertex(13, angpoint(cHoleW, 0));
mesh.setVertex(14, 0.5(fc1.pts[1]+fc2.pts[1]));
mesh.setVertex(15, fc2.pts[1]);
mesh.setVertex(16, segpoint(fc1.sB, 0.2));
mesh.setVertex(17, fc1.pts[1]);
mesh.setVertex(18, fc2.pts[0]);
mesh.setVertex(19, raypoint(cHoleW, fc2.pts[0]));
mesh.setVertex(20, raypoint(cHoleW, fc1.pts[0]));
mesh.setVertex(21, fc1.pts[0]);
mesh.setVertex(22, point(aW, midarc));
mesh.setVertex(23, angpoint(cHoleW, 180));

//---------------------------------------------------------
// Drawing
//---------------------------------------------------------
mesh.draw(defaultpen+2);
mesh.labelVertices(fontsize(18));

pen p = blue+2;
fc1.draw(p);
fc2.draw(p);
draw(aW,p);
draw(aN,p);
draw(cHoleN,p);
draw(cHoleW,p);
