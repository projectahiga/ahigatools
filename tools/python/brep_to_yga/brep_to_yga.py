import rhinoscriptsyntax as rs
import Rhino
import os

surfaces = rs.ObjectsByType(16, select=False)
brep = rs.coercebrep(surfaces[0])

fout = open('output.yga','w')
fout.write('Dimension: 2\n')
fout.write('GeomModel:\n')
fout.write('  Topology:\n')
fout.write('    Nodes:\n')

vertices = brep.Vertices
for v in vertices:
  fout.write('      N%d: [%s]\n' % (v.VertexIndex, v.Location))
fout.write('    Cells:\n')
faces = brep.Faces
for f in faces:
  fout.write('      C%d: [' % (f.FaceIndex))
  ol = f.OuterLoop
  trims = ol.Trims
  for i in [0,1,2]:
    if trims[i].IsReversed():
      fout.write('%d, ' % trims[i].Edge.EndVertex.VertexIndex)
    else:
      fout.write('%d, ' % trims[i].Edge.StartVertex.VertexIndex)
  if trims[3].IsReversed():
    fout.write('%d' % trims[3].Edge.EndVertex.VertexIndex)
  else:
    fout.write('%d' % trims[3].Edge.StartVertex.VertexIndex)
  fout.write(']\n')
fout.write('  Geometry:\n')
fout.write('    Curves:\n')
i = 0
for e in brep.Edges:
  print e.ProxyCurveIsReversed
  fout.write('      K%d:\n' % i)
  fout.write('        Edge: [N%d, N%d]\n' % (e.StartVertex.VertexIndex, e.EndVertex.VertexIndex))
  fout.write('        NURBS:\n')
  fout.write('          ControlPts:\n')
  curve = e.ToNurbsCurve()
  pts = curve.Points
  weights = []
  for p in pts:
    fout.write('          - [%s]\n' % p.Location)
    weights.append(p.Weight)
  fout.write('          Knots: [0.0, ')
  knots = curve.Knots
  start = knots[0]
  end = knots[knots.Count-1]
  for j in range(knots.Count-1):
    fout.write('%s, ' % ((-start+knots[j])/(end-start)))
  fout.write('%s, 1.0]\n' % ((-start+knots[knots.Count-1])/(end-start)))
  fout.write('          Weights: [%s]\n' % ','.join(map(str,weights)))
  print curve
  i = i + 1
  
fout.write('MaterialModel:\n  {}\n')
fout.close()